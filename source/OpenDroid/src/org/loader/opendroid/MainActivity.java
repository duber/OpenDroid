package org.loader.opendroid;

import android.app.Activity;
import android.os.Bundle;

// 测试代码
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/**
		 * step 1 : 插入一条数据， 查询一条数据
		 */
//		Student stu = new Student();
//		stu.setStuName("亓斌");
//		stu.setStuAge(18);
//		stu.save();
//		
//		Student result = OpenDroid.query.find(Student.class, 1);
//		System.out.println(result.getStuName());
		
		/**
		 * step 2 : 查询所有记录
		 */
//		for(int i=0;i<10;i++) {
//			Student stu = new Student();
//			stu.setStuAge(20 + i);
//			stu.setStuName("亓斌" + i);
//			stu.save();
//		}
//		
//		List<Student> result = OpenDroid.query.find(Student.class);
//		for(Student res : result) {
//			System.out.println(res.getStuName());
//		}
		
		/**
		 * step 3 : 查询多条记录
		 */
//		List<Student> result = OpenDroid.query.find(Student.class, 1, 5, 10);
//		for(Student res : result) {
//			System.out.println(res.getId() + " : " + res.getStuName());
//		}
		
		/**
		 * step 4 : 条件查询
		 */
//		List<Student> result = OpenDroid.query.columns("stuName", "stuAge")
//				.where("_id>?", "5").order("_id DESC").limit(3)
//				.find(Student.class);
//		for(Student res : result) {
//			System.out.println(res.getStuName() + " : " + res.getStuAge());
//		}
		
		/**
		 * step 5 : 更新数据
		 */
//		Student stu = new Student();
//		stu.setStuName("loader");
//		stu.setStuName("loader");
//		stu.update(4);
//		stu.update("_id>?", "4");
//		
//		List<Student> result = OpenDroid.query.find(Student.class);
//		for(Student res : result) {
//			System.out.println(res.getStuName());
//		}
		
		/**
		 * step 6 : 使用ContentValues更新
		 */
//		ContentValues cv = new ContentValues();
//		cv.put("stuName", "opendroid");
//		OpenDroid.update(Student.class, cv, "_id=?", "8");
//		
//		Student result = OpenDroid.query.find(Student.class, 8);
//		System.out.println(result.getStuName());
		
		/**
		 * step 7 : 特定删除
		 */
//		int length = OpenDroid.delete(Student.class, 1, 2, 3);
//		System.out.println(length);
		
		/**
		 * step 8 : 使用条件删除
		 */
//		int length = OpenDroid.delete(Student.class, "_id>?", "5");
//		System.out.println(length);
	}
}
